return {
  "neovim/nvim-lspconfig",
  opts = {
    diagnostics = {
      virtual_text = {
        spacing = 10,
        source = "if_many",
        prefix = "●",
      },
      underline = {
        severity = { min = vim.diagnostic.severity.HINT },
      },
      signs = true,
      update_in_insert = false,
    },
    document_highlight = {
      enabled = true,
    },
    codelens = {
      enable = true,
    },
  },
}
