return {
  "git@gitlab.com:gitlab-org/editor-extensions/gitlab.vim.git",
  branch = "angelo",
  event = { "BufReadPre", "BufNewFile" },
  ft = { "go", "typescript", "javascript", "python", "ruby", "lua" }, -- Add the filetypes you want to support
  cond = function()
    -- Only activate if token is present in environment variable.
    -- Remove this line to use the interactive workflow.
    return vim.env.GITLAB_TOKEN ~= nil and vim.env.GITLAB_TOKEN ~= ""
  end,
  opts = {
    statusline = {
      enabled = true,
    },
    code_suggestions = {
      auto_filetypes = { "go", "typescript", "javascript", "python", "ruby", "lua" }, -- Match with ft above
    },
  },
}
