return {
  "nvim-neo-tree/neo-tree.nvim",
  opts = {
    window = {
      position = "left",
      width = 30,
    },
    enable_git_status = true,
    enable_diagnostics = true,
    default_component_configs = {
      indent = {
        padding = 0,
        with_expanders = false,
      },
    },
    filesystem = {
      filtered_items = {
        visible = true,
        hide_dotfiles = false,
        hide_gitignored = false, -- Show gitignored files
        hide_hidden = false,
      },
      hijack_netrw_behavior = "open_default", -- or "open_current"
    },
    source_selector = {
      winbar = false,
      statusline = false,
    },
  },

  -- Set custom highlight for gitignored files
  vim.api.nvim_set_hl(0, "NeoTreeGitIgnored", { fg = "#808080" }),
}
