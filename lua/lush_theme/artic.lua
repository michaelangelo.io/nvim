-- lua/lush_theme/arctic.lua
local lush = require("lush")
local hsl = lush.hsl

local colors = {
  red = hsl("#F44747"),
  yellow = hsl("#FFCC66"),
  blue = hsl("#4FC1FF"),
  green = hsl("#B8CC52"),
  gray = hsl("#D4D4D4"),
  black = hsl("#1E1E1E"),
}

local theme = lush(function()
  return {
    Normal({ bg = colors.black, fg = colors.gray }),
    DiagnosticError({ fg = colors.red, guisp = "undercurl", sp = colors.red }),
    DiagnosticWarn({ fg = colors.yellow, guisp = "undercurl", sp = colors.yellow }),
    DiagnosticInfo({ fg = colors.blue, guisp = "undercurl", sp = colors.blue }),
    DiagnosticHint({ fg = colors.green, guisp = "undercurl", sp = colors.green }),

    -- Add other highlight groups as needed
  }
end)

return theme
