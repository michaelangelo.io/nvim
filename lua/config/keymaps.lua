-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here

vim.api.nvim_set_keymap("n", "<Leader>t", ":terminal<CR>", { noremap = true, silent = true })

vim.api.nvim_set_keymap(
  "n",
  "<leader>ns",
  "<cmd>lua require('package-info').show({ force = true })<cr>",
  { silent = true, noremap = true }
)

vim.api.nvim_set_keymap(
  "n",
  "<leader>pwd",
  ':lua print("Current file path: " .. vim.fn.expand("%:p"))<CR>',
  { noremap = true, silent = true }
)

vim.keymap.set("i", "<C-d>", function()
  local new_text = vim.fn.input("Replace with?: ")
  local cmd = "normal! *Ncgn" .. new_text
  vim.cmd(cmd)
end, { desc = "ctrl+d vs code alternative" })

vim.api.nvim_set_keymap(
  "n",
  "<leader>Y",
  'gg"+yG',
  { noremap = true, silent = true, desc = "Copy entire file to clipboard" }
)
