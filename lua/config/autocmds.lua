-- Autocmds are automatically loaded on the VeryLazy event
-- Default autocmds that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/autocmds.lua
-- Add any additional autocmds here
if vim.g.vscode then
  return
else
  local autocmd = vim.api.nvim_create_autocmd
  autocmd({ "InsertLeave", "TextChanged" }, {
    pattern = { "*" },
    command = "silent! wall",
    nested = true,
  })

  vim.g.autoformat = false

  vim.opt.clipboard = ""

  -- vim.defer_fn(function()
  --   vim.cmd("Copilot auth")
  -- end, 500)

  autocmd("Filetype", {
    pattern = { "*" },
    callback = function()
      local special_filetypes = { "typescript", "css", "python" }
      if vim.tbl_contains(special_filetypes, vim.bo.filetype) then
        vim.opt_local.formatoptions:remove("r") -- don't enter comment leader on Enter in special filetypes
      end
      vim.opt.formatoptions = vim.opt.formatoptions
        + {
          o = false, -- Don't continue comments with o and O
        }
    end,
    desc = "Don't continue comments with o and O, special handling for specific filetypes",
  })

  require("gitlab").setup()
end
-- vim.cmd("GitLabCodeSuggestionsStart")
