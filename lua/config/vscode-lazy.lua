local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not (vim.uv or vim.loop).fs_stat(lazypath) then
  -- bootstrap lazy.nvim
  -- stylua: ignore
  vim.fn.system({ "git", "clone", "--filter=blob:none", "https://github.com/folke/lazy.nvim.git", "--branch=stable", lazypath })
end
vim.opt.rtp:prepend(lazypath)

-- Leader key
vim.g.mapleader = " "
-- Keybindings
local function map(mode, lhs, rhs, opts)
  local options = { noremap = true, silent = true }
  if opts then
    options = vim.tbl_extend("force", options, opts)
  end
  vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

-- Toggle file search
map("n", "<leader>f", '<Cmd>lua require("vscode").action("workbench.action.quickOpen")<CR>')

-- Switch to other buffer/tab
map("n", "<leader>,", '<Cmd>lua require("vscode").action("workbench.action.quickSwitchWindow")<CR>')

-- Buffer actions (as seen in the image)
map("n", "<leader>b", '<Cmd>lua require("vscode").action("workbench.action.showAllEditors")<CR>')
map("n", "<leader>bd", '<Cmd>lua require("vscode").action("workbench.action.closeActiveEditor")<CR>')
map("n", "<leader>bD", '<Cmd>lua require("vscode").action("workbench.action.closeOtherEditors")<CR>')
map("n", "<leader>bp", '<Cmd>lua require("vscode").action("workbench.action.pinEditor")<CR>')
map("n", "<leader>bP", '<Cmd>lua require("vscode").action("workbench.action.unpinEditor")<CR>')

-- Window navigation
map("n", "<leader>wh", '<Cmd>lua require("vscode").action("workbench.action.focusLeftGroup")<CR>')
map("n", "<leader>wj", '<Cmd>lua require("vscode").action("workbench.action.focusBelowGroup")<CR>')
map("n", "<leader>wk", '<Cmd>lua require("vscode").action("workbench.action.focusAboveGroup")<CR>')
map("n", "<leader>wl", '<Cmd>lua require("vscode").action("workbench.action.focusRightGroup")<CR>')

-- Toggle file explorer
map("n", "<leader>e", '<Cmd>lua require("vscode").action("workbench.view.explorer")<CR>')

-- Code actions
map("n", "<leader>ca", '<Cmd>lua require("vscode").action("editor.action.sourceAction")<CR>')

-- Show line diagnostics
map("n", "<leader>d", '<Cmd>lua require("vscode").action("editor.action.showHover")<CR>')

-- Run code lens
map("n", "<leader>cl", '<Cmd>lua require("vscode").action("codelens.showLensesInCurrentLine")<CR>')

-- Rename
map("n", "<leader>cr", '<Cmd>lua require("vscode").action("editor.action.rename")<CR>')

-- Format
-- Format using VSCode's default formatter, ensuring to use the default one
map('n', '<leader>f', '<Cmd>lua require("vscode").action("editor.action.formatDocument")<CR>')


-- LSP references/definitions
map("n", "gr", '<Cmd>lua require("vscode").action("editor.action.goToReferences")<CR>')
map("n", "gd", '<Cmd>lua require("vscode").action("editor.action.revealDefinition")<CR>')

-- Aerial (Symbols)
map("n", "<leader>s", '<Cmd>lua require("vscode").action("outline.focus")<CR>')

-- Additional useful VSCode actions
map("n", "<leader>gg", '<Cmd>lua require("vscode").action("workbench.view.scm")<CR>') -- Open source control
map("n", "<leader>ff", '<Cmd>lua require("vscode").action("workbench.action.findInFiles")<CR>') -- Find in files
map("n", "<leader>tt", '<Cmd>lua require("vscode").action("workbench.action.terminal.toggleTerminal")<CR>') -- Toggle terminal

vim.api.nvim_exec([[
    " THEME CHANGER
    function! SetCursorLineNrColorInsert(mode)
        " Insert mode: blue
        if a:mode == "i"
            call VSCodeNotify('nvim-theme.insert')

        " Replace mode: red
        elseif a:mode == "r"
            call VSCodeNotify('nvim-theme.replace')
        endif
    endfunction

    augroup CursorLineNrColorSwap
        autocmd!
        autocmd ModeChanged *:[vV\x16]* call VSCodeNotify('nvim-theme.visual')
        autocmd ModeChanged *:[R]* call VSCodeNotify('nvim-theme.replace')
        autocmd InsertEnter * call SetCursorLineNrColorInsert(v:insertmode)
        autocmd InsertLeave * call VSCodeNotify('nvim-theme.normal')
        autocmd CursorHold * call VSCodeNotify('nvim-theme.normal')
        autocmd ModeChanged [vV\x16]*:* call VSCodeNotify('nvim-theme.normal')
    augroup END
]], false)


require("lazy").setup({
  spec = {
    { "LazyVim/LazyVim", import = "lazyvim.plugins" },
    { import = "lazyvim.plugins.extras.vscode" },
  },
})
