# VSCode styled 💤 LazyVim

<img src="/assets/preview.png" alt="Preview Image" width="50%">

This configuration uses LazyVim.
Refer to the [documentation](https://lazyvim.github.io/installation) to understand how LazyVim works.

# Installation Steps

1. [Install Neovim](https://neovim.io/)
1. I recommend iTerm2 as a terminal, but others are good as well.
1. Follow the installation steps for [LazyVim](https://lazyvim.github.io/installation)
1. Install nerd fonts: <https://github.com/ryanoasis/nerd-fonts?tab=readme-ov-file#font-installation> (these types of fonts allow icons to be displayed in your file tree (neoTree))
1. Clone this repo to `~.config/nvim`

# Basic Usage

By default, the leading key (denoted `<leader>`) is `space`

### Open File Tree

Similar to the file explorer in VSCode. [NeoTree](https://github.com/nvim-neo-tree/neo-tree.nvim).

> `<leader>E`

### Open Lazy Git

This setup comes with [lazygit](https://github.com/kdheepak/lazygit.nvim). This is similar to the source control tab in VSCode.

> `<leader>lg`

### Package Info

Comes with [package-info.nvim](https://github.com/vuki656/package-info.nvim) for displaying outdated packages.
Open up any `package.json` and run > `<leader>ns`

### Open Terminal Session

> `<leader>t`

### Theme

This comes with [artic.nvim (v2)](https://github.com/rockyzhang24/arctic.nvim/tree/v2). I like this personally because this is what I was used to in VSCode, and it comes with [semantic tokens](https://microsoft.github.io/language-server-protocol/specifications/lsp/3.17/specification/#textDocument_semanticTokens).

These will look different in the editor.

![image](./assets/semantic_tokens.png)

### Print Open Buffer (file) Path

> `<leader>pwd`

## Code

Open code action panel:

> `<leader>c`

### Rename Code

> `<leader>cr`
